#!/usr/bin/env python

"""
Preprocess the SemCor corpus.
"""

import pdb
import os
import argparse
from bs4 import BeautifulSoup
from annotation import write_dataset, Dataset, Document, Sentence, Token, Phrase


def annotate_sentence(doc, begin, sent_text, sent_id):
    sent = Sentence()
    sent.sent_id = sent_id
    sent.begin = begin
    sent.end = begin + len(sent_text)
    sent.doc = doc
    doc.sents.append(sent)
    assert sent.text == sent_text
    return sent

def reannotate_discontinuous_phrase(phrase, begin, chunk_tag):
    end = begin + len(chunk_tag.text)
    phrase.offsets.append([begin, end])
    return phrase


def annotate_discontinuous_phrase(token, begin, phrase_id, chunk_tag):
    phrase = Phrase()
    phrase.phrase_id = phrase_id
    end = begin + len(chunk_tag.text)
    phrase.offsets = [[token.begin, token.end], [begin, end]]
    phrase.sent = token.sent
    token.sent.phrases.append(phrase)
    return phrase


def annotate_continuous_phrase(doc, sent, begin, phrase_id, phrase_tag):
    phrase = Phrase()
    phrase.phrase_id = phrase_id
    end = begin + len(phrase_tag.text)
    phrase.offsets = [[begin, end]]
    phrase.sent = sent
    sent.phrases.append(phrase)
    if phrase_tag.name == 'wf':
        if 'lemma' in phrase_tag.attrs:
            phrase.lemma = phrase_tag.attrs['lemma'].replace('_', ' ')
            assert 'lexsn' in phrase_tag.attrs
            phrase.sense_id = '{}%{}'.format(
                phrase_tag.attrs['lemma'], phrase_tag.attrs['lexsn'])
    return phrase


def annotate_phrase_token(doc, sent, phrase, begin, token_id, token_text):
    token = Token()
    token.token_id = token_id
    token.begin = begin
    token.end = begin + len(token_text)
    token.phrase = phrase
    phrase.tokens.append(token_id)
    token.sent = sent
    sent.tokens.append(token)
    token.doc = doc
    assert token.text == token_text
    return token


def annotate_token(doc, sent, begin, token_id, token_tag):
    token = Token()
    token.token_id = token_id
    token.begin = begin
    token.end = begin + len(token_tag.text)
    if token_tag.name == 'wf':
        if 'pos' in token_tag.attrs:
            token.pos = token_tag.attrs['pos']
        if 'lemma' in token_tag.attrs:
            token.lemma = token_tag.attrs['lemma'].replace('_', ' ')
            assert 'lexsn' in token_tag.attrs
            token.sense_id = '{}%{}'.format(
                token_tag.attrs['lemma'], token_tag.attrs['lexsn'])
    token.sent = sent
    sent.tokens.append(token)
    token.doc = doc
    assert token.text == token_tag.text
    return token


def preprocess(semcor_dir, log_interval=10):
    print(f"Preprocessing {semcor_dir} ...")
    dataset = Dataset()
    for doc_idx, f in enumerate(sorted(os.listdir(semcor_dir))):
        input_file = os.path.join(semcor_dir, f)
        fin = open(input_file, 'r')
        bs = BeautifulSoup(fin, 'html.parser')
        fin.close()

        doc = Document()
        doc.doc_id = bs.find('context')['filename']
        sent_texts = []
        for sent_tag in bs.find_all('s'):
            sent_text = sent_tag.text.replace('_', ' ').replace('\n', ' ').strip()
            sent_texts.append(sent_text)
        doc.text = '\n'.join(sent_texts)

        sent_id = 1
        begin = 0
        for sent_tag in bs.find_all('s'):
            sent_text = sent_tag.text.replace('_', ' ').replace('\n', ' ').strip()
            sent = annotate_sentence(doc, begin, sent_text, sent_id)
            sent_id += 1
            token_id = phrase_id = 1
            chunks = []
            for i, chunk_tag in enumerate(sent_tag.find_all()):  # either phrase or token
                if i > 0:
                    begin += 1  # whitespace

                if 'dc' in chunk_tag.attrs:  # discontinuous phrases
                    prev_chunk = chunks[int(chunk_tag.attrs['dc'])]
                    if isinstance(prev_chunk, Phrase):
                        # Re-annotate the previous phrase
                        phrase = reannotate_discontinuous_phrase(prev_chunk, begin, chunk_tag)
                    else:
                        # Newly annotate a phrase with the previous token
                        phrase = annotate_discontinuous_phrase(prev_chunk, begin, phrase_id, chunk_tag)
                        phrase_id += 1
                    chunks.append(phrase)
                    for j, token_text in enumerate(chunk_tag.text.split('_')):
                        if j > 0:
                            begin += 1  # whitespace
                        token = annotate_phrase_token(doc, sent, phrase, begin, token_id, token_text)
                        token_id += 1
                        begin = token.end
                else:
                    if '_' in chunk_tag.text:  # continuous phrases
                        phrase = annotate_continuous_phrase(doc, sent, begin, phrase_id, chunk_tag)
                        chunks.append(phrase)
                        phrase_id += 1
                        for j, token_text in enumerate(chunk_tag.text.split('_')):
                            if j > 0:
                                begin += 1  # whitespace
                            token = annotate_phrase_token(doc, sent, phrase, begin, token_id, token_text)
                            token_id += 1
                            begin = token.end
                    else:  # tokens
                        token = annotate_token(doc, sent, begin, token_id, chunk_tag)
                        chunks.append(token)
                        token_id += 1
                        begin = token.end

            begin += 1  # linebreak

        dataset.docs.append(doc)
        if (doc_idx + 1) % log_interval == 0:
            print("Processed {} documents.".format(doc_idx + 1))

    print("Processed {} documents.".format(len(dataset.docs)))
    return dataset


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', type=str, metavar='str', default='out/semcor.json.gz',
                        help="output file")
    parser.add_argument('--log-interval', type=str, metavar='str', default=10,
                        help="log interval (default: 10)")
    args = parser.parse_args()

    if not os.path.exists('out'):
        os.mkdir('out')

    dataset1 = preprocess('semcor3.0/brown1/tagfiles', args.log_interval)
    dataset2 = preprocess('semcor3.0/brown2/tagfiles', args.log_interval)
    dataset = Dataset()
    dataset.docs.extend(dataset1.docs)
    dataset.docs.extend(dataset2.docs)

    write_dataset(dataset, args.output)
    print("Saved output to {}".format(args.output))
