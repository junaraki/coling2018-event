#!/usr/bin/env bash


# Download SemCor 3.0
wget http://web.eecs.umich.edu/~mihalcea/downloads/semcor/semcor3.0.tar.gz
tar xfz semcor3.0.tar.gz


# Preprocessing
python semcor.py
python depparse.py


# Event detection
python rule.py --input out/semcor_corenlp.json.gz --output out/semcor_corenlp_rule.json.gz
